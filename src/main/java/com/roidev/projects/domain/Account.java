package com.roidev.projects.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Account {

	private long id;
	private String username;
	@JsonIgnore
	private String password;
	private String email;

	private User user;

	public Account(){
		
	}
	public Account(long id, String username, String password, String email) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
