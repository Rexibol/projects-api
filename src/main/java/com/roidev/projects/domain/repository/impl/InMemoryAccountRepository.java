package com.roidev.projects.domain.repository.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.roidev.projects.domain.Account;
import com.roidev.projects.domain.User;
import com.roidev.projects.domain.repository.AccountRepository;

@Repository
public class InMemoryAccountRepository implements AccountRepository {

	private List<Account> accounts = new ArrayList<>();

	public InMemoryAccountRepository() {

		User user = new User(1, "Konrad", "Kmiecik");
		Account account = new Account(1, "kkmiecik", "password", "konradk92@gmail.com");
		account.setUser(user);
		accounts.add(account);
	}

	@Override
	public List<Account> getAllAccounts() {
		return accounts;
	}

	@Override
	public Account createAccount(Account account) {
		accounts.add(account);
		return account;
	}

}
