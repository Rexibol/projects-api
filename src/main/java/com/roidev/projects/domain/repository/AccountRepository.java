package com.roidev.projects.domain.repository;

import java.util.List;

import com.roidev.projects.domain.Account;

public interface AccountRepository {

	List<Account> getAllAccounts();
	Account createAccount(Account account);

}
