package com.roidev.projects.service;

import com.roidev.projects.domain.Account;

public interface AccountService {

	Account create(Account account);
	
}
