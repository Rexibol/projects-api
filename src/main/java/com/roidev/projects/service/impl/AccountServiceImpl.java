package com.roidev.projects.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.roidev.projects.domain.Account;
import com.roidev.projects.domain.repository.AccountRepository;
import com.roidev.projects.service.AccountService;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	AccountRepository accountRepository;

	@Override
	public Account create(Account account) {
		return accountRepository.createAccount(account);
	}

}
