package com.roidev.projects.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.roidev.projects.domain.Account;
import com.roidev.projects.service.AccountService;

@RestController
public class AccountController {

	@Autowired
	private AccountService accountService;

	/*
	 * @RequestMapping(value = "/account/", method = RequestMethod.GET) public
	 * ResponseEntity<List<Account>> listAllAccounts() { return new
	 * ResponseEntity<List<Account>>(accountRepository.getAllAccounts(),
	 * HttpStatus.OK); }
	 */

	@RequestMapping(value = "/account/", method = RequestMethod.POST)
	public @ResponseBody Account createAccount(@RequestBody Account account) {
		return accountService.create(account);
	}
}
